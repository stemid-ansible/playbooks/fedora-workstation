# Fedora Atomic Workstation

This is my process for setting up a new Fedora Atomic workstation after a fresh install.

## Install bootstrap packages in toolbox container

    toolbox enter
    sudo dnf install python3 python3-pip python3-libdnf git direnv

## Install Ansible

    pip3 install --user ansible

## Clone this repo

    mkdir -p ~/Ansible/playbooks && cd $_
    git clone https://gitlab.com/stemid/fedora-workstation && cd fedora-workstation

## Run Ansible

**Exit the toolbox container** and you should now have the ansible executable in your ``$PATH`` under ``~/.local/bin/ansible``.

The goal of host.yml is to do everything we can do on the host before the first reboot.

    ansible-playbook -K -i inventory/main/hosts host.yml

After this you should reboot into the new ostree layer before running other playbooks.

    systemctl reboot

## Configure repo with direnv

We're done with the host so do the rest in toolbox.

    toolbox enter

Create the file ``.envrc`` with the following data.

    export ANSIBLE_INVENTORY=inventory/main/hosts

Then allow the directory in direnv.

    direnv allow .

## Toolbox

This sets up my basic toolbox utilities, there could be other playbooks for more specific toolbox containers.

    ansible-playbook toolbox.yml

### Known issue: sudo password required

Toolbox containers use an empty sudo password which might cause issues with Ansible. For now my workaround is to run any sudo command first and then run Ansible. I think this creates a cached sudo token that Ansible can use.

I try to avoid editing the sudoers config, where you should actually add NOPASSWD.

## Best practices

* Install as little as possible on the host system.
* Any python, rust, golang or similar packages can be built/installed in toolbox and made available to the host through ``$PATH``.
* Only a package that requires some sort of hardware access, that toolbox cannot provide, needs to be installed on the host.
* Some special cases might be strace, nmap and tcpdump for example, because you want to troubleshoot PIDs on the host, or make privileged network calls.
* Future configuration changes to Sway, Tmux or other programs can be deployed by using tags: ``ansible-playbook -t sway host.yml``

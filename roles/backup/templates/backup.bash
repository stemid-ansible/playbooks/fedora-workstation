#!/usr/bin/env bash
# Perform my personal backup routine to an encrypted USB disk.
# by Stefan Midjich <swehack at gmail dot com>

# Be extra verbose so we can better tell what's going on.
set -xeu

# Configure this to your own USB disk. (ls -l /dev/disk/by-id)
diskId="{{ backup_usb_disk_name }}"
mountPath="/run/media/$(whoami)/Backup"

# Figure out the device path from the disk name.
diskPath="/dev/disk/by-id/$diskId"
test -L "$diskPath" || exit 0 # Exit if disk does not exist.
diskDev=$(readlink -f "$diskPath")

# Unlock encrypted device, and mount resulting mapped device.
output=$(udisksctl unlock -b "$diskDev")
outputArray=($output)
luksDisk="${outputArray[-1]/\./}"
udisksctl mount -b "$luksDisk"

cleanup() {
  udisksctl unmount -b "$luksDisk"
  udisksctl lock -b "$diskDev"
}

trap cleanup ERR EXIT

tar -caf "$mountPath/gnupg/gnupg-$(date -In).tar.zstd" ~/.gnupg
find "$mountPath/gnupg/" -name "gnupg-*.tar.zstd" -type f -ctime +365 -delete 2>/dev/null || true

# The git repo for my password store is on the USB disk.
pushd ~/.password-store
git push
popd

# Test if restic repo exists.
test -d "$mountPath/$(hostname -s)-restic" || exit 1

# This is a workaround for an issue I'm having with running pass through toolbox.
toolbox run pass "$(hostname -s)/restic" > /dev/null

# Run restic with its own setup.
toolbox run restic_backup.sh

toolbox run lsd -lA "$mountPath"
df -h "$mountPath"

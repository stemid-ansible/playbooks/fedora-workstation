#!/usr/bin/env bash
# by Stefan Midjich <swehack at gmail dot com>

set -x

# Hard code it instead of using hostname -s because the script might run from a container.
computerName={{ restic_computer_name }}

if [ -n "$1" ]; then
    export RESTIC_REPOSITORY="s3:https://s3.amazonaws.com/restic-34reu8h4rt8ui7ugtzxds/$computerName"
    max_size=1G
else
    export RESTIC_REPOSITORY="/run/media/stemid/Backup/$computerName-restic"
    if ! [ -d "$RESTIC_REPOSITORY" ]; then
        exit 1
    fi
    max_size=5G
fi

restic backup \
    --exclude-file ~/.restic-excludes \
    --exclude-larger-than "$max_size" \
    --password-command "pass $computerName/restic 2>/dev/null" \
    "${excludes[@]}" \
    .

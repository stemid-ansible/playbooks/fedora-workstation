alias tma="tmux attach -t"
alias tmn="tmux new -s"
alias tmd="tmux detach -s"
alias tml="tmux ls"
alias dnstoys="dig +short @dns.toys"
if [ -f /run/.containerenv ]; then
  alias resticrestore="restic --password-command 'pass $(hostname -s)/restic' restore -t ."
else
  alias govc="podman run --rm --name govc --network host --env-host docker.io/vmware/govc /govc"
  alias aws="podman run --rm --name awscli \
    -e AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY \
    -e AWS_S3_BUCKET \
    -e AWS_DEFAULT_REGION \
    -v '$HOME/.aws:/root/.aws:Z' \
    -i docker.io/amazon/aws-cli"
  alias jq="podman run --rm --name jq -i ghcr.io/jqlang/jq:1.7"
  alias ignition-validate="podman run -i --rm --name ignition-validate quay.io/coreos/ignition-validate:release"
  alias ssh-audit="podman run -i --rm --name ssh-audit docker.io/positronsecurity/ssh-audit"
  alias lsd='podman exec -it -u $USER -w $PWD fedora-toolbox-41 lsd'
  alias bat='podman exec -it -u $USER -w $PWD fedora-toolbox-41 bat'
  alias glab='podman exec -it -u $USER -w $PWD fedora-toolbox-41 glab'
fi
alias mbsync="mbsync -c $HOME/.config/isync/mbsyncrc"

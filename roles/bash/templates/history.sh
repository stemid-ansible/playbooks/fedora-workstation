test -d "$HOME/.bash/history" || mkdir -p "$HOME/.bash/history"
shopt -s histappend
export HISTFILE="$HOME/.bash/history/$(date +'%B').txt"
export HISTSIZE=-1
export HISTFILESIZE=-1
export HISTCONTROL=ignoredups
export FZF_DEFAULT_OPTS="--history=$HISTFILE"

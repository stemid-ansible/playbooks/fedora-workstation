# 0640 for files and 0750 for dirs
umask 027

# Stops bash from saving commands with a leading whitepsace in history
export HISTCONTROL="ignoreboth"

# This stops a security risk with lesspipe
unset LESSOPEN
unset LESSCLOSE

shopt -s histappend
PROMPT_COMMAND="history -s; $PROMPT_COMMAND"

# prompts
git_prompt_script=/usr/share/git-core/contrib/completion/git-prompt.sh
test -r $git_prompt_script && source $git_prompt_script

export PS1='[\u@\h \W](\!/$?$(__git_ps1 "/%s"))$ '

# Autocomplete known ssh hosts
if [[ -e $HOME/.ssh/config ]]; then
  complete -W "$(grep -h '^Host ' $HOME/.ssh/config $HOME/.ssh/*.conf | sort -u | sed 's/^Host //')" ssh autossh
fi

EDITOR=/usr/bin/vim
export PATH=$PATH:$HOME/.npm-packages/bin:$HOME/go/bin:$HOME/.cargo/bin

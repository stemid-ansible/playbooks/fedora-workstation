#!/usr/bin/env bash
# Check if user is Idle and send a notification to remind the user to stretch.
#
# by Stefan Midjich <swehack [at] gmail [punkt] com> 2022/11

set -x

userState=$(loginctl -p State --val show-user $USER)
userIdle=$(loginctl -p IdleHint --val show-user $USER)

if [ "$userState" != 'active' ]; then
  exit 0
fi

if [ "$userIdle" != 'no' ]; then
  exit 0
fi

# Only critical urgency is shown over a fullscreen gnome-terminal
notify-send -t 10000 -u critical Health 'Get up and do your stretches now!'
